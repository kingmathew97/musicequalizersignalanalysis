
$(document).ready(function (e) {
    $("#loading").hide();
    $("#form").on('submit', function (e) {
        $("#loading").show();
        e.preventDefault();        
        $.ajax({
            type: 'POST',
            url: 'https://kingsofdevelopment.com/signals/upload.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (msg) {
                $("#loading").hide();
                if (msg == 'OK') {
                    console.log("Archivo subido correctamente");
                    alert("Archivo subido correctamente");
                } else {
                    console.log("Problemas al subir el archivo");
                    alert("Problemas al subir el archivo");
                }

            }
        });
    });

});

